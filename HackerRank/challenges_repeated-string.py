# https://www.hackerrank.com/challenges/repeated-string/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup&h_r=next-challenge&h_v=zen

# pattern = input()
# length = int(input())
pattern = "aba"
length = 10000000000

pattern_count, tail_length = divmod(length, len(pattern))
a_count = (pattern.count("a") * pattern_count) + pattern[:tail_length].count("a")
print(a_count)
