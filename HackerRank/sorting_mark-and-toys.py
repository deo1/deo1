"""
https://www.hackerrank.com/challenges/mark-and-toys/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting&h_r=next-challenge&h_v=zen
"""


def pick_items(budget, items):
    items = sorted(items)
    running_sum = 0
    num_items = 0
    for item in items:
        running_sum += item
        if running_sum > budget:
            break
        else:
            num_items += 1
    return num_items


if __name__ == "__main__":
    n, budget = (int(from_in) for from_in in input().strip().split())
    items = [int(from_in) for from_in in input().strip().split()]

    num_items = pick_items(budget, items)
    print(num_items)
