# https://www.hackerrank.com/challenges/ctci-bubble-sort/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=sorting

#!/bin/python3

import math
import os
import random
import re
import sys


def swap(a, index):
    a[index], a[index + 1] = a[index + 1], a[index]


# Complete the countSwaps function below.
def countSwaps(a):
    num_swaps = 0
    for _ in range(len(a)):
        for jj in range(len(a) - 1):
            if a[jj] > a[jj + 1]:
                num_swaps += 1
                swap(a, jj)
    return num_swaps, a[0], a[-1]


if __name__ == "__main__":
    n = int(input())
    a = list(map(int, input().rstrip().split()))
    swaps, first, last = countSwaps(a)
    print("Array is sorted in {} swaps.".format(swaps))
    print("First Element: {}".format(first))
    print("Last Element: {}".format(last))
