# https://www.hackerrank.com/challenges/jumping-on-the-clouds/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=warmup

# n = int(input())
# clouds = [c.strip() for c in input() if c.strip() != '']
n = 7
clouds = ["0", "0", "1", "0", "0", "1", "0"]

can_hop = lambda ii, arr: arr[ii] == "0" if ii < len(arr) and ii >= 0 else False

hops, ii = 0, 0
while True:
    if can_hop(ii + 2, clouds):
        hops, ii = hops + 1, ii + 2
    elif can_hop(ii + 1, clouds):
        hops, ii = hops + 1, ii + 1
    elif ii == n - 1:
        break
    else:
        raise Exception

print(hops)
