import numpy as np

classifier_config_dict = {
    # Preprocesssors
    "sklearn.preprocessing.Binarizer": {"threshold": np.arange(0.0, 1.01, 0.05)},
    "sklearn.decomposition.FastICA": {"tol": np.arange(0.0, 1.01, 0.05)},
    "sklearn.cluster.FeatureAgglomeration": {
        "linkage": ["ward", "complete", "average"],
        "affinity": ["euclidean", "l1", "l2", "manhattan", "cosine", "precomputed"],
    },
    "sklearn.preprocessing.MaxAbsScaler": {},
    "sklearn.preprocessing.MinMaxScaler": {},
    "sklearn.preprocessing.Normalizer": {"norm": ["l1", "l2", "max"]},
    "sklearn.kernel_approximation.Nystroem": {
        "kernel": [
            "rbf",
            "cosine",
            "chi2",
            "laplacian",
            "polynomial",
            "poly",
            "linear",
            "additive_chi2",
            "sigmoid",
        ],
        "gamma": np.arange(0.0, 1.01, 0.05),
        "n_components": range(1, 11),
    },
    "sklearn.decomposition.PCA": {
        "svd_solver": ["randomized"],
        "iterated_power": range(1, 11),
    },
    "sklearn.preprocessing.PolynomialFeatures": {
        "degree": [2],
        "include_bias": [False],
        "interaction_only": [False],
    },
    "sklearn.kernel_approximation.RBFSampler": {"gamma": np.arange(0.0, 1.01, 0.05)},
    "sklearn.preprocessing.RobustScaler": {},
    "sklearn.preprocessing.StandardScaler": {},
    "tpot.builtins.ZeroCount": {},
    # Selectors
    "sklearn.feature_selection.SelectFwe": {
        "alpha": np.arange(0, 0.05, 0.001),
        "score_func": {"sklearn.feature_selection.f_classif": None},
    },
    "sklearn.feature_selection.SelectPercentile": {
        "percentile": range(1, 100),
        "score_func": {"sklearn.feature_selection.f_classif": None},
    },
    "sklearn.feature_selection.VarianceThreshold": {
        "threshold": np.arange(0.05, 1.01, 0.05)
    },
    "sklearn.feature_selection.RFE": {
        "step": np.arange(0.05, 1.01, 0.05),
        "estimator": {
            "sklearn.ensemble.ExtraTreesClassifier": {
                "n_estimators": [100],
                "criterion": ["gini", "entropy"],
                "max_features": np.arange(0.05, 1.01, 0.05),
            }
        },
    },
    "sklearn.feature_selection.SelectFromModel": {
        "threshold": np.arange(0, 1.01, 0.05),
        "estimator": {
            "sklearn.ensemble.ExtraTreesClassifier": {
                "n_estimators": [100],
                "criterion": ["gini", "entropy"],
                "max_features": np.arange(0.05, 1.01, 0.05),
            }
        },
    },
}
