import numpy as np

from sklearn.ensemble import ExtraTreesClassifier
from sklearn.feature_selection import RFE
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import BernoulliNB
from sklearn.pipeline import make_pipeline

# NOTE: Make sure that the class is labeled 'class' in the data file
tpot_data = np.recfromcsv(
    "PATH/TO/DATA/FILE", delimiter="COLUMN_SEPARATOR", dtype=np.float64
)
features = np.delete(
    tpot_data.view(np.float64).reshape(tpot_data.size, -1),
    tpot_data.dtype.names.index("class"),
    axis=1,
)
training_features, testing_features, training_target, testing_target = train_test_split(
    features, tpot_data["class"], random_state=42
)

exported_pipeline = make_pipeline(
    RFE(
        estimator=ExtraTreesClassifier(
            criterion="entropy", max_features=0.6000000000000001, n_estimators=100
        ),
        step=0.7000000000000001,
    ),
    BernoulliNB(alpha=0.01, fit_prior=True),
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
