import numpy as np

from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import MinMaxScaler

# NOTE: Make sure that the class is labeled 'class' in the data file
tpot_data = np.recfromcsv(
    "PATH/TO/DATA/FILE", delimiter="COLUMN_SEPARATOR", dtype=np.float64
)
features = np.delete(
    tpot_data.view(np.float64).reshape(tpot_data.size, -1),
    tpot_data.dtype.names.index("class"),
    axis=1,
)
training_features, testing_features, training_target, testing_target = train_test_split(
    features, tpot_data["class"], random_state=42
)

exported_pipeline = make_pipeline(
    MinMaxScaler(),
    SelectPercentile(score_func=f_classif, percentile=49),
    MultinomialNB(fit_prior=False),
)

exported_pipeline.fit(training_features, training_target)
results = exported_pipeline.predict(testing_features)
