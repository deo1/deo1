import random
from multiprocessing import cpu_count
from time import time
import spacy
import string
from utils import map_reduce
from typing import Optional, Dict, Tuple
from collections import defaultdict

if spacy.__version__ == "1.9.0":
    nlp = spacy.load("en_core_web_md")
else:
    nlp = spacy.load("en")


def randomword(length):
    letters = string.ascii_lowercase
    return "".join(random.choice(letters) for i in range(length))


def randomstring(length):
    return " ".join(randomword(length) for i in range(length))


def spacy_results(tokens: Tuple[spacy.tokens.span.Span]) -> Optional[Dict]:
    if not tokens:
        return None

    token_results = defaultdict(lambda: [])
    for token in tokens:
        token_results[token.label_].append(token.string.strip())

    return dict(token_results) if token_results else None


def nlp_pipe(data, results, id):
    piped = nlp.pipe(data)
    results[id] = [spacy_results(val.ents) for val in piped]
    return results[id]


if __name__ == "__main__":
    samples = 100_000
    test_data = [randomstring(7) for i in range(samples)]
    procs = cpu_count() // 2  # physical cores
    print(f"\nRunning test... ({samples} samples, {procs} processes)")
    print(f"Spacy version: {spacy.__version__}")

    start = time()
    results = map_reduce(test_data, nlp_pipe, procs)
    print(f"Map Reduce Duration: {round(time() - start, 3)} seconds")
    print(f"Result count: {len(results)}\n")

    start = time()
    results_two = nlp_pipe(test_data, {}, 1)
    print(f"Single Threaded Duration: {round(time() - start, 3)} seconds")
    print(f"Result count: {len(results_two)}\n")

    assert results == results_two
