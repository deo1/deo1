import random
import multiprocessing
from time import time


def list_append(count, id, out_list):
    """
    Creates an empty list and then appends a
    random number to the list 'count' number
    of times. A CPU-heavy operation!
    """
    for i in range(count):
        out_list.append(random.random())


if __name__ == "__main__":
    size = 100000000  # Number of random numbers to add
    target = (
        2
    )  # Target number for benchmarking (will run this many sequentially if proc == 1)
    procs = 2  # Number of processes to create
    jobs = []
    for i in range(0, procs):
        out_list = list()
        process = multiprocessing.Process(target=list_append, args=(size, i, out_list))
        jobs.append(process)

    # Start the processes (i.e. calculate the random number lists)
    start = time()
    if procs == 1:
        for i in range(0, target):
            out_list.clear()
            list_append(size, i, out_list)

    elif procs > 1:
        for j in jobs:
            j.start()

        # Ensure all of the processes have finished
        for j in jobs:
            j.join()
    print(f"Duration = {round(time() - start, 3)}")
