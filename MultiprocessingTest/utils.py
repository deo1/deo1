import multiprocessing
from typing import List, Callable, Any, Dict
from uuid import uuid4
from decorator import decorator  # type: ignore

Process = multiprocessing.Process


def _split(data: List, n: int):
    """
    Split list `data` into `n` equal parts.
    Returns a generator object.
    """
    k, m = divmod(len(data), n)
    if k == 0:
        raise ValueError(f"{n} > len(data)")
    return (data[i * k + min(i, m) : (i + 1) * k + min(i + 1, m)] for i in range(n))


def _map(
    data: List,
    func: Callable,  # should be decorated with @multiprocess_function
    procs: int,
    results: Dict,
) -> List[Process]:
    """
    Splits list data up into N parts to be processed
    by each process and creates and send to the user provided function
    """
    jobs = []
    for ii in _split(data, procs):
        process = Process(target=func, args=(ii, results, uuid4()))
        jobs.append(process)

    return jobs


def _reduce(results) -> List:
    """
    Combines the results of the user provided function for each
    mapped data set
    """
    results_out = [inner for outer in results.values() for inner in outer]

    return results_out


def map_reduce(data: List, func: Callable, procs: int) -> List:
    """
    Entry level function for splitting up data evenly across
    processes and applying the supplied user function
    """
    manager = multiprocessing.Manager()
    results = manager.dict()  # global used to store results across processes

    jobs = _map(data, func, procs, results)
    for j in jobs:
        j.start()
    for j in jobs:
        j.join()

    return _reduce(results)
