# reference: https://github.com/deepmind/pysc2
# reference: https://github.com/deo1/pysc2-examples
# reference: https://github.com/openai/baselines/blob/master/baselines/deepq/simple.py
# reference: https://github.com/deo1/terrortoss/blob/master/docs/environment.md

from pysc2 import run_configs
from pysc2.agents import scripted_agent
from pysc2.env import run_loop
from pysc2.env import sc2_env

#from pysc2.env import available_actions_printer
#from pysc2.env import environment
#from pysc2.lib import features
#from pysc2.lib import actions

#import gflags as flags
from time import sleep

#_PLAYER_RELATIVE = features.SCREEN_FEATURES.player_relative.index
#_PLAYER_FRIENDLY = 1
#_PLAYER_NEUTRAL = 3  # beacon/minerals
#_PLAYER_HOSTILE = 4
#_NO_OP = actions.FUNCTIONS.no_op.id
#_MOVE_SCREEN = actions.FUNCTIONS.Move_screen.id
#_ATTACK_SCREEN = actions.FUNCTIONS.Attack_screen.id
#_SELECT_ARMY = actions.FUNCTIONS.select_army.id
#_NOT_QUEUED = [0]
#_SELECT_ALL = [0]

#FLAGS = flags.FLAGS

# constants
STEPS = 3000
STEP_MUL = 1
FRAMETIME_CAP = 5

# map stuff
maps = sc2_env.maps.get_maps();
minigames = { k:v for k, v in maps.items() if v.directory == 'mini_games' }

# run
with run_configs.get().start() as controller:
    minigame_minerals_env = \
        sc2_env.SC2Env(
            map_name='CollectMineralShards',
            agent_race='T', # not needed for mineral shards map, just fyi
            step_mul=STEP_MUL,
            game_steps_per_episode=STEPS * STEP_MUL)

    with minigame_minerals_env as env: # `with` calls `__enter__` then `__exit__` methods
        agent = scripted_agent.CollectMineralShards()

        for ts in run_loop.run_loop([agent], env, STEPS):
            print(ts)
            sleep(FRAMETIME_CAP)
