Implementing a neural net from scratch (NaiveNet.py), including backprop and gradient descent.
Then re-implementing the same from-scratch class interfaces using PyTorch (NaiveTorchNet.py)
