import json
import math
from dataclasses import dataclass, field
from typing import Any, Callable, DefaultDict as DD, Dict, List, Tuple

import numpy as np
import pandas as pd
from scipy import interpolate, signal

pd.set_option("display.max_columns", 50)
pd.set_option("max_colwidth", 100)
pd.set_option("display.width", 250)
pi = np.pi
waveforms = signal.waveforms
repeat = lambda v, l: [v for _ in range(l)]
khz = lambda fs: f"{fs / 1_000:.1f} khz" if fs % 1_000 else f"{fs // 1_000} khz"


def padr(sig: np.ndarray, l: int) -> np.ndarray:
    r = sig.size
    if r > l:
        return sig[:l]
    elif r < l:
        return np.pad(sig, pad_width=(0, l - r), mode="constant")
    else:
        return sig


def padlr(pad: float, shift: int = 0):
    return (max(math.floor(pad) + shift, 0), max(math.ceil(pad) - shift, 0))


@dataclass
class Signal:
    fs: int
    hz: float
    dur: float = 1.0
    A: float = 1.0
    name: str = ""
    waveform: Callable[[Any], np.ndarray] = field(default=waveforms.square, repr=False)
    wfkwargs: Dict[str, Any] = field(default_factory=dict, repr=False)
    rfactor: float = 1.0
    rwindow: Tuple = ("kaiser", 0.0)
    bitdepth: type = np.float32
    resampled: List[int] = field(default_factory=list, repr=False)
    t: np.ndarray = field(default=None, repr=False)
    y: np.ndarray = field(default=None, repr=False)
    error: np.ndarray = field(default=None, repr=False)

    f: Callable[[np.ndarray], np.ndarray] = field(init=False, repr=False)

    @property
    def shape(self) -> Tuple[int, int]:
        return self.y.shape

    @property
    def size(self) -> int:
        return self.y.size

    @property
    def provenance(self) -> str:
        up, dn, p = " ↗ ", " ↘ ", ""
        fsp = None
        for fs in self.resampled[-2:]:
            if fsp:
                p += f"{up}{khz(fs)}" if fs > fsp else f"{dn}{khz(fs)}"
            else:
                p += f"{khz(fs)}"
            fsp = fs
        return f"{p.replace(' khz', '')} khz"

    @property
    def fs_khz(self) -> str:
        return khz(self.fs)

    @property
    def wave(self) -> str:
        return f"{self.waveform.__name__} {self.hz:,.0f} hz"

    @property
    def omega(self) -> float:
        return 2 * pi * self.hz

    @property
    def dict(self) -> Dict[str, List]:
        l = self.size
        return {
            "fs": repeat(self.fs, l),
            "fs_khz": repeat(self.fs_khz, l),
            "fs_beg": repeat(khz(self.resampled[0]), l),
            "hz": repeat(self.hz, l),
            "dur": repeat(self.dur, l),
            "A": repeat(self.A, l),
            "omega": repeat(self.omega, l),
            "name": repeat(self.name, l),
            "waveform": repeat(self.waveform.__name__, l),
            "wave": repeat(self.wave, l),
            "wfkwargs": repeat(json.dumps(self.wfkwargs), l),
            "rfactor": repeat(self.rfactor, l),
            "rwindow": repeat(self.rwindow, l),
            "bitdepth": repeat(self.bitdepth.__name__, l),
            "provenance": repeat(self.provenance, l),
            "rcount": repeat(len(self.resampled) - 1, l),
            "resampled": repeat(json.dumps(self.resampled), l),
            "t": self.t.tolist(),
            "y": self.y.tolist(),
            "error": self.error.tolist(),
        }

    @property
    def df(self) -> pd.DataFrame:
        df = pd.DataFrame(self.dict)
        value_vars = set(["y", "error"])
        id_vars = set(df.columns).difference(value_vars)
        return df.melt(id_vars, value_vars, "signal", "y")

    @property
    def repr(self) -> List[str]:
        return [
            f"fs={self.fs:6,.0f} hz",
            f"waveform={self.waveform.__name__}",
            f"hz={self.hz:3,.0f}",
            f"dur={self.dur:,.1f}",
            f"A={self.A:.1f}",
            f"rfactor={self.rfactor:4.1f}",
            f"rwindow={self.rwindow}",
            f"bitdepth={self.bitdepth.__name__}",
            f"name={self.name}",
            f"provenance={self.provenance}",
        ]

    def __post_init__(self):
        self.name = (
            f"{self.waveform.__name__}_{self.hz}" if not self.name else self.name
        )
        self.t = (
            np.linspace(0.0, self.dur, self.dur * self.fs, endpoint=False)
            if self.t is None
            else self.t
        )
        self.y = (
            self.waveform(self.A * self.omega * self.t, **self.wfkwargs).astype(
                self.bitdepth
            )
            if self.y is None
            else self.y
        )
        self.f = interpolate.interp1d(x=self.t, y=self.y)
        self.error = (
            np.nan * np.zeros(self.size, dtype=self.bitdepth)
            if self.error is None
            else self.error
        )
        if not self.resampled:
            self.resampled.append(self.fs)
        assert self.t.size == self.y.size == self.error.size

    def __repr__(self) -> str:
        return f"Signal({', '.join(self.repr)})"

    def __add__(self, o: "Signal") -> "Signal":
        self.y += padr(o.y, self.size)
        return self

    def __sub__(self, o: "Signal") -> "Signal":
        self.y -= padr(o.y, self.size)
        return self

    def resample(self, rfactor: float, rwindow: Tuple = ("kaiser", 0.0)) -> "Signal":
        fs = math.floor(rfactor * self.fs)
        y, t = self.y, self.t
        y, t = signal.resample(
            x=y,
            num=math.floor(fs * self.dur),
            t=t,
            window=signal.get_window(rwindow, t.size),
        )
        yinterp = self.f(np.clip(t, self.t.min(), self.t.max()))  # type: ignore

        return Signal(  # type: ignore
            fs=fs,
            hz=self.hz,
            dur=self.dur,
            A=self.A,
            name=self.name,
            waveform=self.waveform,
            wfkwargs=self.wfkwargs,
            rfactor=rfactor,
            rwindow=rwindow,
            bitdepth=self.bitdepth,
            resampled=self.resampled + [fs],
            t=t,
            y=y,
            error=np.abs(y - yinterp),
        )

    def tslice(self, t0: float, t1: float) -> Tuple[np.ndarray, np.ndarray]:
        fs = self.fs
        i0, i1 = int(fs * t0), int(fs * t1)
        return self.t[i0:i1], self.y[i0:i1]

    def fft(
        self, t0: float = 0.0, s: int = 2 ** 12, win: np.ndarray = None
    ) -> np.ndarray:
        win = signal.windows.hann(s) if win is None else win
        i0 = int(self.fs * t0)
        half = s // 2
        y = self.y[i0 : i0 + s]
        pad = padlr((s - win.size) / 2)
        pwin = np.pad(win, pad, mode="constant")
        fy = np.fft.rfft(pwin * y, norm="ortho")
        fy_real = np.real(fy)
        fy_imag = np.imag(fy)
        fy_mag = np.sqrt(fy_real ** 2 + fy_imag ** 2)
        freq = np.fft.rfftfreq(n=s, d=1 / self.fs)
        return np.asarray([freq, fy_mag, fy_real, fy_imag])

    def fft_df(self, *args, **kwargs) -> pd.DataFrame:
        df = pd.DataFrame(self.fft(**kwargs).T, columns=["freq", "mag", "real", "imag"])
        fsize = (df.shape[0] - 1) * 2
        df["fsize"] = fsize
        df["bsize"] = self.fs / fsize
        df["mag_clip"] = np.clip(df.mag, a_min=5e-1, a_max=np.inf)
        df["wave"] = self.wave
        df["f0_est"] = df.freq.iloc[df.mag.idxmax()]
        df["f0_gt"] = f"{self.hz:,}"
        df["A"] = self.A
        df["omega"] = self.omega
        return df.reset_index().rename(columns={"index": "bin"})


def merge_sig_df(
    sigs: List[Signal], sort_vals: List[str] = ["name", "rcount", "t"]
) -> pd.DataFrame:
    dfs = [s.df for s in sigs]
    return (
        pd.concat(dfs)
        .sort_values("signal", ascending=False)
        .sort_values(sort_vals)
        .reset_index(drop=True)
    )


def fmt_fft_plot(ax, yscale="linear", xscale="linear", size=(16, 6), title=""):
    ax.set_yscale(yscale)
    ax.set_xscale(xscale)
    ax.figure.set_size_inches(size)
    if title:
        ax.set_title(title)
    return ax
